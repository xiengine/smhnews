//
//  Constants.h
//
//  Created by Jonathan Xie
//  Copyright 2014 XIEngine. All rights reserved.

extern NSString * const APP_NAME;
extern NSString * const DATA_URL;
extern NSString * const DEFAULT_THUMBMAIL;

extern int const CELL_CONTENT_WIDTH;
extern int const CELL_CONTENT_MARGIN;
extern int const CELL_CONTENT_PADDING;
extern int const CELL_MARGINS;
extern float const CELL_CONTENT_MARGIN_RIGHT;

extern float const CELL_HEADLINE_LEFT;
extern int const TABLE_CELL_HEIGHT_MIN;

extern int const FONT_SIZE_SMALL;
extern float const FONT_SIZE_LARGER;
extern int const FONT_SIZE_SMALLER;



