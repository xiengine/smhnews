//
//  Article.h
//  SMHNews
//
//  Created by Jonathan Xie on 21/02/2014.
//  Copyright (c) 2014 fairfaxdigital. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Article : NSObject

@property(nonatomic, strong) NSString * headLine;
@property(nonatomic, strong) NSString * slugLine;
@property(nonatomic, strong) NSString * dateLine;
@property(nonatomic, strong) NSString * thumbnailImageHref;
@property(nonatomic, strong) NSString * tinyUrl;
@property(nonatomic, strong) NSString * webHref;
@property(nonatomic, strong) NSString * type;
@property(nonatomic, assign) NSInteger * identifier;

@end
