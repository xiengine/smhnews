//
//  UITableViewCell+Utilities.m
//  SMHNews
//
//  Created by Jonathan Xie on 22/02/2014.
//  Copyright (c) 2014 fairfaxdigital. All rights reserved.
//

#import "UITableViewCell+Utilities.h"

#define kDefaultLabelWidth 240

@implementation UITableViewCell(Utilities)

+ (CGFloat)heightForLabelWithString:(NSString *)pString
{
    CGSize labelSize = [pString sizeWithFont:[UIFont fontWithName:@"HelveticaNeue" size:14.0f]
                           constrainedToSize:CGSizeMake(kDefaultLabelWidth, MAXFLOAT)
                               lineBreakMode:NSLineBreakByWordWrapping];
    return labelSize.height;
}

+ (CGFloat)heightForLabelWithString:(NSString *)pString andWidth:(CGFloat)pWidth
{
    CGSize labelSize = [pString sizeWithFont:[UIFont fontWithName:@"HelveticaNeue" size:14.0f]
                           constrainedToSize:CGSizeMake(pWidth, MAXFLOAT)
                               lineBreakMode:NSLineBreakByWordWrapping];
    return labelSize.height+10;
}
@end

