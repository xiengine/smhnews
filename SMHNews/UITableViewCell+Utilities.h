//
//  UITableViewCell+Utilities.h
//  SMHNews
//
//  Created by Jonathan Xie on 22/02/2014.
//  Copyright (c) 2014 fairfaxdigital. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UITableViewCell(Utilities)

+ (CGFloat)heightForLabelWithString:(NSString *)pString;
+ (CGFloat)heightForLabelWithString:(NSString *)pString andWidth:(CGFloat)pWidth;
@end
