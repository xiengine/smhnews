//
//  ContentViewController.h
//  SMHNews
//
//  Created by Jonathan Xie on 22/02/2014.
//  Copyright (c) 2014 fairfaxdigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContentViewController : UIViewController<UIWebViewDelegate,UIActionSheetDelegate>{
    
}

@property(nonatomic, retain) NSURL *url;
@property(nonatomic, retain) UIWebView *webView;
@property(nonatomic, retain) UIActivityIndicatorView *activityIndicator;


- (id)initWithUrls:(NSURL*)anURL;


@end
