//
//  AppDelegate.h
//  SMHNews
//
//  Created by Jonathan Xie on 21/02/2014.
//  Copyright (c) 2014 fairfaxdigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
