//
//  ArticleTableCell.m
//  SMHNews
//
//  Created by Jonathan Xie on 21/02/2014.
//  Copyright (c) 2014 fairfaxdigital. All rights reserved.
//

#import "ArticleTableCell.h"


#define TEXTVIEW_INSET UIEdgeInsetsMake(-9,-4,0,0)
#define COLOR_TEXT [UIColor colorWithRed:.285 green:.376 blue:.541 alpha:1]
#define COLOR_IMG [UIColor colorWithRed:.285 green:.376 blue:.541 alpha:0.1]

#define RECT_HEADLINE CGRectMake(65.0f, 10.0f, 240.0f, 0.0f)
#define RECT_SLUGLINE CGRectMake(65.0f, 40.0f, 240.0f, 0.0f)
#define RECT_DATELINE CGRectMake(65.0f, 0.0f, 120.0f, 16.0f)
#define RECT_THUMBNAIL CGRectMake(5.0f, 10.0f, 50.0f, 34.0f)


@interface ArticleTableCell()

@property (nonatomic, assign) BOOL hasImage;

@end

@implementation ArticleTableCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        //setup initial subviews
        [self addSubViews];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

-(void)addSubViews
{
    //headLine
    _headLine = [[[UITextView alloc] initWithFrame:RECT_HEADLINE] autorelease];
    [_headLine setBackgroundColor:[UIColor clearColor]];
    [_headLine setTextColor:COLOR_TEXT];
    [_headLine setFont:[UIFont fontWithName:@"HelveticaNeue" size:FONT_SIZE_LARGER]];
    _headLine.editable = NO;
    [_headLine setUserInteractionEnabled:NO];
    _headLine.contentInset = TEXTVIEW_INSET;
    [self addSubview:_headLine];
    
    //image
    _thumbnail = [[[UIImageView alloc] initWithFrame:RECT_THUMBNAIL] autorelease];
    _thumbnail.backgroundColor = COLOR_IMG;
    _thumbnail.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:_thumbnail];
    
    //slugLine
    _slugLine = [[[UITextView alloc] initWithFrame:RECT_SLUGLINE] autorelease];
    [_slugLine setBackgroundColor:[UIColor clearColor]];
    [_slugLine setTextColor:COLOR_TEXT];
    [_slugLine setFont:[UIFont fontWithName:@"Helvetica" size:FONT_SIZE_SMALL]];
    _slugLine.editable = NO;
    [_slugLine setUserInteractionEnabled:NO];
    _slugLine.contentInset = TEXTVIEW_INSET;
    [self addSubview:_slugLine];
    
    //dateLine
    _dateLine = [[[UILabel alloc] initWithFrame:RECT_DATELINE] autorelease];
    [_dateLine setBackgroundColor:[UIColor clearColor]];
    [_dateLine setTextColor:COLOR_TEXT];
    [_dateLine setFont:[UIFont fontWithName:@"Helvetica" size:FONT_SIZE_SMALLER]];
    [self addSubview:_dateLine];
    
}

- (void)layoutSubviews {
    
    [super layoutSubviews];
    
    CGRect fullScreenRect = [[UIScreen mainScreen] bounds];
    
    //on orientation, resize subviews
    if ([UIDevice currentDevice].orientation!=UIDeviceOrientationLandscapeLeft &&
        [UIDevice currentDevice].orientation!=UIDeviceOrientationLandscapeRight)
    {
        CGRect slugeFrame = _slugLine.frame;
        slugeFrame.size.width = CELL_CONTENT_WIDTH;
        _slugLine.frame = slugeFrame;
        
    }else{
        
        CGRect slugeFrame = _slugLine.frame;
        slugeFrame.size.width = fullScreenRect.size.height - CELL_CONTENT_MARGIN_RIGHT;
        _slugLine.frame = slugeFrame;
    }

    
    //re-position dateLine
    CGRect dateFrame = _dateLine.frame;
    dateFrame.origin.y = _headLine.frame.origin.y + _headLine.frame.size.height - (CELL_CONTENT_PADDING+CELL_CONTENT_MARGIN);
    _dateLine.frame = dateFrame;
    
    
    //re-position slugLine
    CGRect slugFrame = _slugLine.frame;
    slugFrame.origin.y = _dateLine.frame.origin.y + _dateLine.frame.size.height + CELL_CONTENT_PADDING;
    _slugLine.frame = slugFrame;
    
}





+ (CGFloat)heightForCell:(NSString *)headLine slugLine:(NSString*)slugLine
{
    CGRect fullScreenRect = [[UIScreen mainScreen] bounds];
    float contentWidth;
    
    //
    if ([UIDevice currentDevice].orientation!=UIDeviceOrientationLandscapeLeft &&
        [UIDevice currentDevice].orientation!=UIDeviceOrientationLandscapeRight)
    {
        contentWidth = CELL_CONTENT_WIDTH;
    }else{
        contentWidth = fullScreenRect.size.height - CELL_CONTENT_MARGIN_RIGHT;
    }
    
    CGSize headLineSize = [headLine sizeWithFont:[UIFont fontWithName:@"HelveticaNeue" size:FONT_SIZE_LARGER]
                               constrainedToSize:CGSizeMake(contentWidth, MAXFLOAT)
                                   lineBreakMode:NSLineBreakByWordWrapping];
    
    CGSize slugLineSize = [slugLine sizeWithFont:[UIFont fontWithName:@"Helvetica" size:FONT_SIZE_SMALL]
                               constrainedToSize:CGSizeMake(contentWidth, MAXFLOAT)
                                   lineBreakMode:NSLineBreakByWordWrapping];
    
    //
    return headLineSize.height + slugLineSize.height + CELL_MARGINS;
    
}




- (void)updateTextView:(NSString *)content textView:(UITextView *)textView {
    //we need this bit to fix iOS 7 weird stuff
    [textView setScrollEnabled:YES];
    [textView setText:content];
    [textView sizeToFit];
    [textView setScrollEnabled:NO];
    //
    CGRect frame = textView.frame;
    frame.size.width = CELL_CONTENT_WIDTH;
    textView.frame = frame;
}

-(void)updateDate:(NSString*)aDate
{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:SSZZZZ"];
    NSDate *theDate = [formatter dateFromString:aDate];
    //
    [formatter setDateFormat:@"dd/MM/yyyy HH:mm"];
    _dateLine.text  = [formatter stringFromDate:theDate];
    
    [formatter release];
    
}

-(void)updateImage:(UIImage*)aImage
{
    _thumbnail.hidden = NO;
    _thumbnail.image = aImage;
    [self setNeedsLayout];
    
    //this test block's supposed to support missed image.
    //guess it's bad UX on design. so go with default image instead!
    /*
    if(aImage!=nil){
        _thumbnail.hidden = NO;
        _thumbnail.image = aImage;
        
        //re-position headLine
        CGRect headFrame = _headLine.frame;
        headFrame.origin.x = 65;
        headFrame.size.width = 240;
        _headLine.frame = headFrame;
        
        //re-position dateLine
        CGRect dateFrame = _dateLine.frame;
        dateFrame.origin.x = 65;
        dateFrame.size.width = 240;
        _dateLine.frame = dateFrame;
        
        //re-position slugLine
        CGRect slugeFrame = _slugLine.frame;
        slugeFrame.origin.x = 65;
        slugeFrame.size.width = 240;
        _slugLine.frame = slugeFrame;
        
    }else{
        //has no image!
        _thumbnail.hidden = YES;
        
        //re-position headLine
        CGRect headFrame = _headLine.frame;
        headFrame.origin.x = 10;
        headFrame.size.width = 300;
        _headLine.frame = headFrame;
        
        //re-position dateLine
        CGRect dateFrame = _dateLine.frame;
        dateFrame.origin.x = 10;
        dateFrame.size.width = 300;
        _dateLine.frame = dateFrame;
        
        //re-position slugLine
        CGRect slugeFrame = _slugLine.frame;
        slugeFrame.origin.x = 10;
        slugeFrame.size.width = 300;
        _slugLine.frame = slugeFrame;
        
    }
    
    [self setNeedsLayout];
     */
}









- (void)dealloc {
    [_headLine release], _headLine=nil;
    [_slugLine release], _slugLine=nil;
    [_thumbnail release], _thumbnail=nil;
    [_dateLine release], _dateLine=nil;
    
    [super dealloc];
}

@end
