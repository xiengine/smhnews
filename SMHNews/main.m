//
//  main.m
//  SMHNews
//
//  Created by Jonathan Xie on 21/02/2014.
//  Copyright (c) 2014 fairfaxdigital. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
