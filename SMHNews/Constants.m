//
//  Constants.m
//
//  Created by Jonathan Xie
//  Copyright 2014 XIEngine. All rights reserved.
//

#import "Constants.h"

NSString * const APP_NAME = @"Latest News";
NSString * const DATA_URL = @"http://mobilatr.mob.f2.com.au/services/views/9.json";
NSString * const DEFAULT_THUMBMAIL = @"fairfaxmedia.jpg";

int const CELL_CONTENT_WIDTH = 240;
int const CELL_CONTENT_MARGIN = 5;
int const CELL_CONTENT_PADDING = 10;
float const CELL_CONTENT_MARGIN_RIGHT = 80;
int const TABLE_CELL_HEIGHT_MIN = 44;
float const CELL_HEADLINE_LEFT = 65.0f;
int const CELL_MARGINS = 60;

int const FONT_SIZE_SMALL = 11;
int const FONT_SIZE_SMALLER = 8;
float const FONT_SIZE_LARGER = 14.0f;



