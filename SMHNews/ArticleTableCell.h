//
//  ArticleTableCell.h
//  SMHNews
//
//  Created by Jonathan Xie on 21/02/2014.
//  Copyright (c) 2014 fairfaxdigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArticleTableCell : UITableViewCell

@property (nonatomic, retain) UITextView *headLine;
@property (nonatomic, retain) UITextView *slugLine;
@property (nonatomic, retain) UIImageView * thumbnail;
@property (nonatomic, retain) UILabel *dateLine;


- (void)updateTextView:(NSString *)content textView:(UITextView *)textView;
- (void)updateDate:(NSString*)aDate;
- (void)updateImage:(UIImage*)aImage;
+ (CGFloat)heightForCell:(NSString *)headLine slugLine:(NSString*)slugLine;


@end
