//
//  ContentViewController.m
//  SMHNews
//
//  Created by Jonathan Xie on 22/02/2014.
//  Copyright (c) 2014 fairfaxdigital. All rights reserved.
//

#import "ContentViewController.h"

@interface ContentViewController ()

@end

@implementation ContentViewController

- (id)initWithUrls:(NSURL*)anURL
{
    self = [self init];
    if(self)
    {
        self.url = anURL;
    }
    
    return self;
}

- (void)dealloc
{
    [_webView release],_webView=nil;
    [_url release],_url=nil;
    [_activityIndicator release],_activityIndicator=nil;
    
    [super dealloc];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.title = @"";
    
	//
    self.webView = [[[UIWebView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)] autorelease];
    self.webView.dataDetectorTypes = UIDataDetectorTypeNone;
    self.webView.scalesPageToFit = YES;
    self.webView.delegate = self;
    self.webView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:self.webView];
    
    if(self.url){
        [self.webView loadRequest:[NSURLRequest requestWithURL:self.url]];
    }
    
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.activityIndicator.center = CGPointMake(160, 240);
    self.activityIndicator.hidesWhenStopped = YES;
    [self.view addSubview:self.activityIndicator];
    
    [self.activityIndicator release];
    
    

}

#pragma mark -
#pragma mark webView


- (void)webViewDidStartLoad:(UIWebView *)webView {
	[self.activityIndicator startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [self.activityIndicator stopAnimating];
}



- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    return YES;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration
{
    CGRect fullScreenRect = [[UIScreen mainScreen] bounds];
    
    if( interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown){
        self.webView.frame = CGRectMake(0, 0, fullScreenRect.size.width, fullScreenRect.size.height);
    }
    if( interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight) {
        self.webView.frame = CGRectMake(0, 0, fullScreenRect.size.height, fullScreenRect.size.width);
    }
}





@end
