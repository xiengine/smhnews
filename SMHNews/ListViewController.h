//
//  ListViewController.h
//  SMHNews
//
//  Created by Jonathan Xie on 21/02/2014.
//  Copyright (c) 2014 fairfaxdigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@end
