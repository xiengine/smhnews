//
//  ListViewController.m
//  SMHNews
//
//  Created by Jonathan Xie on 21/02/2014.
//  Copyright (c) 2014 fairfaxdigital. All rights reserved.
//

#import "ListViewController.h"
#import "ArticleTableCell.h"
#import "ContentViewController.h"


@interface ListViewController ()

@property(nonatomic, strong) UITableView * tableView;
@property(nonatomic, strong) NSMutableArray * jsonArray;
@property(nonatomic, strong) NSCache * imageCache;
@end

@implementation ListViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = APP_NAME;
    
    //tableview setup
    self.tableView = [[[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height) style:UITableViewStylePlain] autorelease];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:self.tableView];
    
    //setup cache
    self.imageCache = [[[NSCache alloc] init] autorelease];
    [self.imageCache setCountLimit:24];
    
}


-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    //
    [self fetchDataFromURL:DATA_URL];
    
}


-(void)fetchDataFromURL:(NSString*)aURL
{
    // fetch store data on a separate thread
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
        NSURL *url = [NSURL URLWithString:aURL];
        NSData *data = [NSData dataWithContentsOfURL:url];
        NSError *jsonError = nil;
        NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonError];
        
        if (jsonData != nil) {
            self.jsonArray = [jsonData objectForKey:@"items"];
            
            // refresh table with store data from network
            [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
        }
        else {
            // error msg
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:@"Error loading data"
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
            [alert show];
        }
    });
}




#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.jsonArray count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"articleCellIdentifier";
    
    ArticleTableCell *cell = (ArticleTableCell *)[_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[ArticleTableCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    //make sure we are not blocking the main thread and loading data lazily
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
        //0. data parsing
        NSString * aHeadLine = [[[NSString alloc] initWithFormat:@"%@", [[_jsonArray objectAtIndex:indexPath.row] objectForKey:@"headLine"]] autorelease];
        
        NSString * aSlugLine = [[[NSString alloc] initWithFormat:@"%@", [[_jsonArray objectAtIndex:indexPath.row] objectForKey:@"slugLine"]] autorelease];
        
        NSString * aDateLine = [[[NSString alloc] initWithFormat:@"%@", [[_jsonArray objectAtIndex:indexPath.row] objectForKey:@"dateLine"]] autorelease];
        
        NSString * thumbnailURL = [[[NSString alloc] initWithFormat:@"%@", [[_jsonArray objectAtIndex:indexPath.row] objectForKey:@"thumbnailImageHref"]] autorelease];
        
        NSString * identifier = [[[NSString alloc] initWithFormat:@"id_%@", [[_jsonArray objectAtIndex:indexPath.row] objectForKey:@"identifier"]] autorelease];
        
        //1. text
        dispatch_async(dispatch_get_main_queue(), ^{
            [cell updateTextView:aHeadLine textView:cell.headLine];
            [cell updateTextView:aSlugLine textView:cell.slugLine];
            [cell updateDate:aDateLine];
            [cell setNeedsLayout];
            
        });
        
        //2. image
        UIImage *cachedImage = [_imageCache objectForKey:identifier];
        
        //try cache image first
        if (cachedImage) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [cell updateImage:cachedImage];
            });
        } else {
            //otherwise load remotely
            if (![thumbnailURL isEqual:[NSNull null]]){
                NSURL * imageURL =  [NSURL URLWithString:thumbnailURL];
                NSData *imageData = [[[NSData alloc] initWithData:[NSData dataWithContentsOfURL:imageURL]] autorelease];
                UIImage *thumbnail = [[UIImage alloc] initWithData:imageData];
                //
                if (thumbnail) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [cell updateImage:thumbnail];
                    });
                    //cache image
                    [self.imageCache setObject:thumbnail forKey:identifier];
                }
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    //default
                    [cell updateImage:[UIImage imageNamed:DEFAULT_THUMBMAIL]];
                });
            }
            
        }

        
    });
    
    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    //measure custom cell height
    NSString * headLine = [[NSString alloc] initWithFormat:@"%@", [[_jsonArray objectAtIndex:indexPath.row] objectForKey:@"headLine"]];
    NSString * slugLine = [[NSString alloc] initWithFormat:@"%@", [[_jsonArray objectAtIndex:indexPath.row] objectForKey:@"slugLine"]];
    
    CGFloat cellHeight = [ArticleTableCell heightForCell:headLine slugLine:slugLine];
    CGFloat height = MAX(cellHeight, TABLE_CELL_HEIGHT_MIN);
    
    
    return height;
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    //
    NSString * href = [[[NSString alloc] initWithFormat:@"%@", [[_jsonArray objectAtIndex:indexPath.row] objectForKey:@"webHref"]] autorelease];
    
    
    
    ContentViewController * nextView = [[ContentViewController alloc] initWithUrls:[NSURL URLWithString:href]];
	
	[self.navigationController pushViewController:nextView animated:YES];
    [nextView release];

}


//support orientation
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration
{
    CGRect fullScreenRect = [[UIScreen mainScreen] bounds];
    
    if( interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown){
        self.tableView.frame = CGRectMake(0, 0, fullScreenRect.size.width, fullScreenRect.size.height);
    }
    if( interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight) {
        self.tableView.frame = CGRectMake(0, 0, fullScreenRect.size.height, fullScreenRect.size.width);
    }
    
    //force the custom cell to refresh on orientation
    [self.tableView reloadData];
}




- (void)dealloc {
    [_tableView release], _tableView=nil;
    [_jsonArray release], _jsonArray=nil;
    [_imageCache release], _imageCache=nil;
    
    [super dealloc];
}


@end



